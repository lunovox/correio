modCorreio.openjornalbroadcast = function(playername, message)
	--if player~=nil and player:is_player() then
	if type(playername)=="string" and playername~="" then
		--local playername = player:get_player_name()
		if modCorreio.mailbox[playername]==nil then
			modCorreio.mailbox[playername] = { }
			modCorreio.mailbox[playername].selmail = 0
		end
		if message == nil then
		   message = ""
	   end
		local formspecmails = modCorreio.get_formspecmails(playername)
		--print("formspecmails = "..formspecmails)
		--[[ 
		local formspec = "size[8,6.0]"
		.."image[2.2,0.0;0.7,0.7;obj_mail.png]"
		--.."label[2.75,0;"..minetest.formspec_escape(modCorreio.translate("MAIL LETTER")).."]"
		--.."field[0.5,1.0;7.7,1.0;toplayer;"..minetest.formspec_escape(modCorreio.translate("Addressee"))..":;]"
		.."textarea[0.5,0;7.7,5.5;txtJornal;"
         ..minetest.formspec_escape(modCorreio.translate("Message"))..":;"
         ..minetest.formspec_escape(message)
      .."]"
		.."label[0.35,4.75;"..minetest.formspec_escape(modCorreio.translate("\\n = enter | \\t = tab")).."]"
		.."button_exit[3.0,5.5;2.5,0.5;btnSendJornal;"..minetest.formspec_escape(modCorreio.translate("PUBLISH")).."]"
		--]]
		
		
		local label = "Message published in the newspaper"
		local formspec = "size[10,8.0]"
		.."bgcolor[#00880044;false]"
		.."image[0.25,0.25;0.5,0.5;obj_mail.png]"
		.."box[0,0;9.75,7.25;#000000CC]"
		.."label[0.86,0.25;"
			..minetest.formspec_escape(
				core.colorize("#00FFFF", 
					modCorreio.translate(
						label
					)..":"
				)
			)
      .."]"
		.."textarea[0.5,0.75;9.5,6.75;txtJornal;;"..minetest.formspec_escape(message).."]"
		.."label[0.35,6.6;"..minetest.formspec_escape(modCorreio.translate("\\n = enter | \\t = tab")).."]"
		.."button_exit[6.75,7.6;3,0.5;btnSendJornal;"..minetest.formspec_escape(modCorreio.translate("PUBLISH")).."]"
		
		minetest.show_formspec(playername,"modCorreio.frmJornal",formspec)
	end
end

minetest.register_on_player_receive_fields(function(sender, formname, fields)
	if formname == "modCorreio.frmJornal"  then
		local sendername = sender:get_player_name()
		--print()
		--minetest.log('action',"[modCorreio] fields = "..dump(fields))
		--[[ 
		minetest.chat_send_all(
		   "item_jornal.lua >>> "
		   .."register_on_player_receive_fields >>> "
		   .."sender= "..dump(sendername)
		   .." | formname = "..dump(formname)
   		.." | fields = "..dump(fields)
   	)
   	--]]
		
		if fields.btnSendJornal and fields.quit then
			if fields.txtJornal ~= nil 
			   and type(fields.txtJornal) == "string" 
			   and fields.txtJornal:trim() ~= "" 
			then
			   minetest.log(
			      'action',(
			         "[correio] Player '%s' sended a jornal publisher, that say: \n%s"
			      ):format(sendername, fields.txtJornal)
			   )
	         local contsend = modCorreio.chat_broadcast(sendername, fields.txtJornal)
	         if contsend < 1 then
	            modCorreio.openjornalbroadcast(playername, fields.txtJornal)
            end
			else
				minetest.chat_send_player(sendername, 
					core.colorize("#FF0000", "[CORREIO:ERRO] ")..
					modCorreio.translate("Please enter a message before sending your jornal!")
				)
			end
		end
	end
end)

