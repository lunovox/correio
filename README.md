![screenshot]

# [CORREIO] 📨
[[Minetest Mod]]

Adds mailbox and mail paper that allow ordinary players to send messages to each other even if the recipient is not online. The player who receives a message will be warned with a letter symbol on his screen.

### **Dependencies:**

  * default (Minetest Game Included)
  * dye (Minetest Game Included)
  * [lib_savevars] → Library to save variables per others mods in file ````variables.tbl````.

### **Optional Dependencies:**

  * [intllib] → Facilitates the translation of several others mods into your native language, or other languages.
  * [brazutec] → Mod that adds a laptop to the minetest game

### **Licence:**

* GNU AGPL: https://gitlab.com/lunovox/correio/-/raw/master/LICENSE

More details: 
 * English: https://en.wikipedia.org/wiki/GNU_Affero_General_Public_License
 * Portuguese: https://pt.wikipedia.org/wiki/GNU_Affero_General_Public_License

### **Download/Repository:**

* https://gitlab.com/lunovox/correio

### **Developers:**

* Lunovox Heavenfinder: [email][lunomail], [xmpp][lunoxmpp], [social web][lunosocialweb], [audio conference][lunoconf], [more contacts][lunomore]

[lunomail]:mailto:lunovox@disroot.org
[lunoxmpp]:xmpp:lunovox@disroot.org?join
[lunosocialweb]:http://mastodon.social/@lunovox
[lunoconf]:https://meet.jit.si/MinetestBrasil
[lunomore]:https://libreplanet.org/wiki/User:Lunovox

### **Settings:**

Change the file **'minetest.conf'** to change the initial settings of the mod, such as:

* ````correio.hud.delay = 10```` →  Delay in seconds of notice of arrival of the new message. Need to be longer than 5 seconds to not cause lag on server
* ````correio.max_validate_days = 30```` → Limit how long a mail can be stored inside the mailbox. This prevents emails from players that did abandon the server.

### **Privileges:**

* ````postman```` →? Allows to publish messages to everyone on the server.
* ````walkingreader```` → Allows you to read your incoming emails using commands (without using a mailbox).
* ````walkingwriter```` → Allows you to write email using commands (without using a letter).

### **Commands:**

 * ````/broadcastmail <mensagem>```` or ````/bcm <mensagem>```` → Send email to all registered players. You need priv 'postman'.
 * ````/deloldsmail```` or ````/dom```` → Delete emails that are more than 30 days old on the whole server. Serves to relieve server memory. You need priv 'postman'.
 * ````/sendmail [<ToPlayer> <Message>]```` or ````/sm [<ToPlayer> <Message>]```` → If you do not enter parameters you will open a panel to fill the recipient and the message you want to send. You need priv 'postman'.
 * ````/readmail```` or ````/rm```` → Displays incoming messages. You need priv 'walkingreader'.
 * ````/clearmails```` or ````/cm```` → Delete all emails already read by the player. You need priv 'walkingreader'.

### **Translate to Others Languages:**

* This mod currently are configured to language:
	* English
	* Portuguese

* To add a new language to this mod just follow the steps below:
	* Enable the complementary mod **'intllib'.**
	* Set your language in **'minetest.conf'** by adding the [````language = <your language>````] property. 
		* Example for French Language: ````language = fr````
	* Make a copy of the file [ **pt.txt** ] in the [ **locale** ] folder that is inside the mod for [````locale/<your_language>.txt````]. 
		* Example for French language: ````locale/fr.txt````
	* Open the file [````locale/<your_language>.txt````] in a simple text editor.
	* Translate all lines. But, just here that stems the right of the equals symbol (=). 
		* Example for French Language: ````MAIL LETTER=LETTRE DE COURRIER````

### **API:**
 * ````<table> modCorreio.get_mails(<playername>)```` → Collect mailing list from a given Player.
 * ````<table> modCorreio.get_mail(<playername>, <mailnumber>)```` → Collect a mail of a specific number from a certain Player.
 * ````modCorreio.set_mail(<namefrom>, <nameto>, <message>)```` → Send a publish mail from a Player or NPC to all Players
 * ````modCorreio.set_broadcast(<namefrom>, <message>)```` → Send an email from a Player or NPC to all other Players.
 * ````modCorreio.del_mail(<playername>, <mailnumber>)```` → Deletes an email from a specific number from a certain Player.
 * ````<number> modCorreio.get_countunreaded(<playername>)```` →  Returns the number of emails read from a given Player.
 * ````modCorreio.set_read(<playername>, <mailnumber>, <boolean>)```` → True/false read mark in an email from a specific number from a specific Player.
 * ````modCorreio.del_readeds(<playername>)```` → Deletes all read mails from a specific Player.
 * ````modCorreio.chat_delreadeds(<playername>)```` → Deletes all read mails from a given Player and displays the result in the player's chat.
 * ````modCorreio.del_olds()```` →  Deletes all emails from all players that are more than ````correio.max_validate_days```` number of days old.
 * ````modCorreio.chat_readmail(<playername>)```` → Graphically display the email listing window to a given player.
 * ````modCorreio.hud_print(<playername>)```` → Graphically display the HUD on a player's screen if there are unread emails.
 * ````modCorreio.hud_check()```` → Graphically display the HUD on the screen of all online players if they have unread emails.

[CORREIO]:https://gitlab.com/lunovox/correio
[screenshot]:https://gitlab.com/lunovox/correio/-/raw/master/screenshot.png
[Minetest Mod]:https://www.minetest.net/
[lib_savevars]:https://gitlab.com/Lunovox/lib_savevars
[intllib]:https://github.com/minetest-mods/intllib
[brazutec]:https://github.com/BrunoMine/brazutec

