modCorreio = {
   modName = core.get_current_modname(),
   modPath = core.get_modpath(core.get_current_modname())
}

dofile(modCorreio.modPath.."/translate.lua")
dofile(modCorreio.modPath.."/api.lua")
--dofile(modCorreio.modPath.."/api_md2html.lua")
dofile(modCorreio.modPath.."/chatcommand.lua")
dofile(modCorreio.modPath.."/item_mailbox.lua")
dofile(modCorreio.modPath.."/item_papermail.lua")
dofile(modCorreio.modPath.."/item_jornal.lua")
dofile(modCorreio.modPath.."/item_brazutec.lua")
dofile(modCorreio.modPath.."/item_computing_app.lua")
dofile(modCorreio.modPath.."/item_postman.lua")

core.log('action',"["..modCorreio.modName:upper().."] Carregado!")
