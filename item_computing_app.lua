

if core.global_exists("modComputing") then
   modComputing.add_app("correio:btnOpenInBox", {
   	icon_name = "btnOpenInBox",
   	icon_title = modCorreio.translate("RECEIVEDS"),
   	icon_descryption = modCorreio.translate("Open email inbox."),
   	icon_type = "button", --types: button/button_exit
   	icon_image = "icon_mail2.png",
   	on_iconclick = function(player)
   	   local playername = player:get_player_name()
   	   core.log(
			   "action","["..modCorreio.modName:upper().."] "
			   ..modCorreio.translate(
               "Player '@1' is trying to show the '@2' via the '@3'!"
               , playername
               , "mailbox"
               , "computing app"
            )
         )
			modCorreio.openinbox(playername)
   	end,
   })
   modComputing.add_app("correio:btnSendMail", {
   	icon_name = "btnSendMail",
   	icon_title = modCorreio.translate("SEND"),
   	icon_descryption = modCorreio.translate("Sends email to a specific player."),
   	icon_type = "button", --types: button/button_exit
   	icon_image = "obj_mail.png",
   	is_visible = function(player)
   	   return (
   	      core.check_player_privs(player, {walkingwriter = true})
         )
   	end,
   	on_iconclick = function(player)
   	   local playername = player:get_player_name()
   	   core.log(
			   "action","["..modCorreio.modName:upper().."] "
			   ..modCorreio.translate(
               "Player '@1' is trying to show the '@2' via the '@3'!"
               , playername
               , "papermail sender"
               , "computing app"
            )
         )
			modCorreio.openpapermail(playername)
   	end,
   })
   modComputing.add_app("correio:btnSendJornal", {
   	icon_name = "btnSendJornal",
   	icon_title = modCorreio.translate("BROADCAST"),
   	icon_descryption = modCorreio.translate("Send email to all registered players."),
   	icon_type = "button", --types: button/button_exit
   	icon_image = "obj_mailbox.png",
   	is_visible = function(player)
   	   return (
   	      core.check_player_privs(player, {postman = true})
         )
   	end,
   	on_iconclick = function(player)
   	   local playername = player:get_player_name()
   	   core.log(
			   "action","["..modCorreio.modName:upper().."] "
			   ..modCorreio.translate(
               "Player '@1' is trying to show the '@2' via the '@3'!"
               , playername
               , "broadcast sender"
               , "computing app"
            )
         )
			modCorreio.openjornalbroadcast(playername)
   	end,
   })
end