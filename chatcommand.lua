--COMMAND BROADCAST
minetest.register_chatcommand(
	"broadcastmail", 
	modCorreio.getPropCommBroadcast("broadcastmail")
)
minetest.register_chatcommand(
	"broadcast", 
	modCorreio.getPropCommBroadcast("broadcast")
)
minetest.register_chatcommand(
	"bcmail", 
	modCorreio.getPropCommBroadcast("bcmail")
)
minetest.register_chatcommand(
	"bcm", 
	modCorreio.getPropCommBroadcast("bcm")
)
--######################################

--DELETE OLD MAIL
minetest.register_chatcommand(
	"deloldsmail", 
	modCorreio.getPropCommDeleteOldMails("deloldsmail")
)
minetest.register_chatcommand(
	"delolds", 
	modCorreio.getPropCommDeleteOldMails("delolds")
)
minetest.register_chatcommand(
	"domail", 
	modCorreio.getPropCommDeleteOldMails("domail")
)
minetest.register_chatcommand(
	"dom", 
	modCorreio.getPropCommDeleteOldMails("dom")
)
--######################################

--SEND MAIL
minetest.register_chatcommand(
	"sendmail", 
	modCorreio.getPropCommSendMail("sendmail")
)
minetest.register_chatcommand(
	"mail", 
	modCorreio.getPropCommSendMail("mail")
)
minetest.register_chatcommand(
	"sm", 
	modCorreio.getPropCommSendMail("sm")
)
--######################################

--READ MAIL
minetest.register_chatcommand(
	"readmail", 
	modCorreio.getPropCommShowMailBox("readmail")
)
minetest.register_chatcommand(
	"read", 
	modCorreio.getPropCommShowMailBox("read")
)
minetest.register_chatcommand(
	"inbox", 
	modCorreio.getPropCommShowMailBox("inbox")
)
minetest.register_chatcommand(
	"rm", 
	modCorreio.getPropCommShowMailBox("rm")
)
minetest.register_chatcommand(
	"ib", 
	modCorreio.getPropCommShowMailBox("ib")
)
--######################################

--CLEAR READED MAILS
minetest.register_chatcommand(
	"clearmails", 
	modCorreio.getPropCommDeleteReadMails("clearmails")
)    
minetest.register_chatcommand(
	"cm", 
	modCorreio.getPropCommDeleteReadMails("cm")
)


