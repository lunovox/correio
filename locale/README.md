# TRANSLATES

To generate file '**template.pot**', did use terminal command:

````bash
cd correio
xgettext -n *.lua -L Lua --force-po --keyword=modCorreio.translate --from-code=UTF-8 -o ./locale/template.pot

````
----

### Cria arquivo .po a partir de um arquivo de templante .pot

Sintaxe:
```bash
msginit --no-translator --no-wrap --locale=$LANG.UTF-8 --output-file=$LANG.po --input=$POT
```
Example to brasilian portuguese language (pt_BR):
```bash
msginit --no-translator --no-wrap --locale=pt_BR.UTF-8 --output-file=./locale/pt_BR.po --input=./locale/template.pot
```
----

### Atualiza arquivo ,po a partir de um arquivo de templante .pot

Sintaxe:
```bash
msgmerge --sort-output --no-wrap --update --backup=off $LANG.po $POT
```
Example to brasilian portuguese language (pt_BR):
```bash
msgmerge --sort-output --no-wrap --update --backup=off ./locale/pt_BR.po ./locale/template.pot
```
----

To edit '**.po**' files to your language use app poedit.:

Example of translation into brasilian portuguese language (pt_BR):
```bash
cd correio
sudo apt-get inatall poedit
poedit ./locale/pt_BR.po
```

* See others locales used: ca;cs;da;de;dv;eo;es;et;fr;hu;id;it;ja;jbo;kn;lt;ms;nb;nl;pl;pt;pt_BR;ro;ru;sl;sr_Cyrl;sv;sw;tr;uk

----

## Convert '.po' file to '.tr' file.

### COMMAND SAMPLE: TRANSLATE TO BRASILIAN PORTUGUESSE
````
cd ./locale/
lua po2tr.lua "correio" "pt_BR.po"
mv "pt_BR.tr" "correio.pt_BR.tr"
cat correio.pt_BR.tr | less
````
----

### TO ENABLE YOUR LANGUAGE IN MINETEST

* To enable the translate to brasilian portuguese language, write ```language = pt_BR``` in file "minetest.conf". Or write the command ```/set -n language pt_BR``` in game chat, and run again the minetest game.

> See more: 
* https://forum.minetest.net/viewtopic.php?f=47&t=21974
* https://github.com/minetest/minetest/issues/8158
* https://gist.githubusercontent.com/mamchenkov/3690981/raw/8ebd48c2af20c893c164e8d5245d9450ad682104/update_translations.sh
* https://gitlab.com/4w/xtend/-/blob/master/xtend_default/tools/convert_po_file_to_tr_file/convert_po_file_to_tr_file.lua


