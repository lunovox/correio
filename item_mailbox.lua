modCorreio.mailbox = { }


modCorreio.openinbox = function(playername)
	--if player ~= nil and player:is_player() then
	if type(playername)=="string" and playername~="" then
		--local playername = player:get_player_name()
		if modCorreio.mailbox[playername]==nil or 
			modCorreio.mailbox[playername].selmail == nil or 
			type(modCorreio.mailbox[playername].selmail)~="number" or 
			modCorreio.mailbox[playername].selmail<0 
		then
			modCorreio.mailbox[playername] = { }
			modCorreio.mailbox[playername].selmail = 0
		end
		
		local formspecmails = modCorreio.get_formspecmails(playername) --<== Retorna os titulos das cartas em formato de formspec
		--minetest.log('action',"formspecmails = "..formspecmails)
		local formspec = "size[8,7.5]"
		.."label[3.25,0;CORREIO]"
		.."textlist[0,0.7;7.7,6;selmail;"..formspecmails..";"..modCorreio.mailbox[playername].selmail..";false]"
		.."button_exit[0.5,7;1.75,0.5;closer;"..minetest.formspec_escape(modCorreio.translate("CLOSE")).."]"
		.."button[2.25,7;1.75,0.5;openmail;"..minetest.formspec_escape(modCorreio.translate("OPEN")).."]"
		.."button[4.0,7;1.75,0.5;delmail;"..minetest.formspec_escape(modCorreio.translate("DELETE")).."]"
		.."button[5.75,7;1.75,0.5;clearmails;"..minetest.formspec_escape(modCorreio.translate("CLEAR")).."]"
		
		minetest.show_formspec(playername,"modCorreio.mailbox",formspec)
	end
end

modCorreio.openmail = function(playername, mailnumber)
	--if player ~= nil and player:is_player() then
		--local playername = player:get_player_name()

	if type(playername)=="string" and playername~="" then
		local mail = modCorreio.get_mail(playername, mailnumber)
		if mail~=nil then
			--local formspecmails = modCorreio.get_formspecmails(playername)
			--minetest.log('action',"formspecmails = "..formspecmails)
			
			local body_message = mail.message
			--body_message = getString_MD2HTML(body_message)
			
			if core.global_exists("libMarkdown2Html") then
			   body_message = libMarkdown2Html.doConverterV2(body_message)
			end
			body_message = minetest.formspec_escape(body_message)
			
			
			minetest.log('action',"[CORREIO] mail = "..dump(mail))
			local formspec = ""
			--.."formspec_version[5]"
			.."formspec_version[6]"
			--.."size[10,7.5]"
			.."size[15,10]"
			.."box[0,0;15,1.0;#0000FFCC]"
			--.. default.gui_bg
			--.. default.gui_bg_img
			.."label[0.5,0.5;"..minetest.formspec_escape(modCorreio.translate("From")..": "..mail.namefrom).."]"
			.."label[7.5,0.5;"..minetest.formspec_escape(modCorreio.translate("When")..": "..os.date("%Y-%m-%d %Hh:%Mm:%Ss", mail.time)).."]"
					.."button[12.5,1.50;2.0,0.5;printer;"..minetest.formspec_escape(modCorreio.translate("PRINTER")).."]"
					.."button[12.5,2.25;2.0,0.5;delmail;"..minetest.formspec_escape(modCorreio.translate("DELETE")).."]"
					.."button[12.5.0,3.00;2.0,0.5;openinbox;"..minetest.formspec_escape(modCorreio.translate("BACK")).."]"
			 .."button_exit[12.5,3.75;2.0,0.5;closer;"..minetest.formspec_escape(modCorreio.translate("CLOSE")).."]"
			
			.."box[0.5,1.5;11.5,8.0;#FFFFFFFF]"
			--[[ 
			.."textarea[0.5,1.5;7.7,6;;"
				..minetest.formspec_escape(modCorreio.translate("Message")..":")..";"
				..minetest.formspec_escape(body_message)
			.."]"
			--]]
			
			--[[ 	--]]
			--button_url[<X>,<Y>;<W>,<H>;<name>;<label>;<url>]
			--.."button_url[0,0;1,8;link_name_and_url;aaaaaa aaaaa;"..minetest.formspec_escape("https://minetest.net").."]"
			--.."button_url[1.5,1.0;2.5,0.8;homepage;minetest.net;https://www.minetest.net/]"
			.."hypertext[0.75,1.75;11.0,7.5;browser;"
				--..minetest.formspec_escape("<global margin=10 valign=0 color=#FF00FF hovercolor=#00FFFF size=12 font=normal halign=center >")
				.."<global valign=top halign=left margin=10 background=#88888800 color=#000000 hovercolor=#00FF00 size=14 font=normal>"

				.."<tag name=h1      background=#888888 color=#0088FF size=32>"		
				.."<tag name=h2      background=#888888 color=#0044FF size=28>"		
				.."<tag name=h3      background=#888888 color=#0022FF size=24>"		
				.."<tag name=h4      background=#888888 color=#0000FF size=20>"		
				.."<tag name=h5      background=#888888 color=#000088 size=16>"		
				
				.."<tag name=p      background=#888888 color=#000000 size=14 halign=justify margin=15>"		
				.."<tag name=red     color=#FF0000>"
				.."<tag name=green   color=#00FF00>"
				.."<tag name=blue    color=#0000FF>"
				.."<tag name=cyan    color=#88CCFF>"
				.."<tag name=yellow  color=#FFFF00>"
				.."<tag name=magenta color=#FF00FF>"
				.."<tag name=pink    color=#FF88FF>"
				.."<tag name=orange  color=#FF8800>"
				.."<tag name=brown   color=#964B00>"
				.."<tag name=black   color=#000000>"
				.."<tag name=grey    color=#888888>"
				.."<tag name=white   color=#FFFFFF>"
				.."<tag name=code   background=#444444 color=#888888 size=16 font=mono>"
				.."<tag name=hidden background=#00000000 color=#00000000 size=10 font=mono>"

				
				.."<tag name=action color=#FF0000 hovercolor=#00FF00 font=normal size=12>"
				--.."<tag name=item valign=top width=32 height=32>"
				..body_message
			.."]" -- Fim de hypertext[]
			--]]
			--..body_message

			
			--.."textlist[0,0.7;15.5,6;selmail;"..formspecmails..";1;false]"

			--[[
<h1>TESTE DE LINK IN EXTERNAL BROWSER</h1>

<action name=link_name>Link Name</action>.
<action name=link_name_and_url url=https://minetest.net>https://minetest.net</action>.
<action url=https://minetest.net>https://minetest.net</action>.

.
			--]]

			modCorreio.set_read(playername, mailnumber, true)
		
			minetest.show_formspec(playername,"modCorreio.mailbox",formspec)
		end
	end
end

minetest.register_on_player_receive_fields(function(sender, formname, fields)
	if formname == "modCorreio.mailbox"  then
		local playername = sender:get_player_name()
		--minetest.log('action',"[CORREIO] fields = "..dump(fields))
		if fields.openinbox then
			minetest.log('action',"modCorreio.openinbox("..playername..")")
			modCorreio.openinbox(playername)
		elseif fields.selmail then
			local selnum = (fields.selmail):gsub("CHG:", "")
			minetest.log('action',"modCorreio.mailbox[playername].selmail="..dump(modCorreio.mailbox[playername].selmail))
			modCorreio.mailbox[playername].selmail = tonumber(selnum)
		elseif fields.openmail~=nil then
			if modCorreio.mailbox[playername].selmail~=nil and type(modCorreio.mailbox[playername].selmail)=="number" and modCorreio.mailbox[playername].selmail >=1 then
				minetest.log('action',"modCorreio.openmail["..playername.."].selmail="..dump(modCorreio.mailbox[playername].selmail))
				modCorreio.openmail(playername, modCorreio.mailbox[playername].selmail)
			else
				minetest.chat_send_player(playername, 
					core.colorize("#FF0000", "[CORREIO:ERRO] ")..
					modCorreio.translate("Select the letter you want to open!")
				)
			end
		elseif fields.delmail~=nil then
			if modCorreio.mailbox[playername].selmail~=nil and type(modCorreio.mailbox[playername].selmail)=="number" and modCorreio.mailbox[playername].selmail >=1 then
				minetest.log('action',"modCorreio.del_mail("..playername..", "..modCorreio.mailbox[playername].selmail..")")
				modCorreio.del_mail(playername, modCorreio.mailbox[playername].selmail)
				modCorreio.openinbox(playername)
			else
				minetest.chat_send_player(playername, 
					core.colorize("#FF0000", "[CORREIO:ERRO] ")..
					modCorreio.translate("Select the letter you want to delete!")
				)
			end
		elseif fields.clearmails~=nil then
			minetest.log('action',"modCorreio.chat_delreadeds("..playername..")")
			modCorreio.chat_delreadeds(playername)
			modCorreio.openinbox(playername)
		else --if fields.browser then
			minetest.log('action',"[CORREIO:"..playername..":action] fields = "..dump(fields))
			--core.open_url("https://www.minetest.net")
			--minetest.log('action',"[correio register_on_player_receive_fields('"..playername.."')]\n>>> fields.browser="..dump(fields.browser))
		--elseif fields.homepage then
			--core.open_url("https://www.minetest.net")
		end
	end
end)

local mailbox_format = {
	type = "fixed",
	fixed = { 
		{-.25,-.25,.5,			.25,.356,.45}
	}
}

--local efectcolor = "^[colorize:#00FF00"
--local efectcolor = "^[invert:rgb"
local efectcolor = "^[multiply:#00FFFF"
modCorreio.tiles = {
	mailbox_private = {
		"tex_mailbox_topdown.png", --cima
		"tex_mailbox_topdown.png", --baixo
		"tex_mailbox_sides.png", --direita
		"tex_mailbox_sides.png^[transformFX", --esquerda
		"tex_mailbox_back.png", --traz
		"tex_mailbox_from.png^tex_mailbox_from_en.png", --frente
		--
	},
	mailbox_public = {
		"tex_mailbox_topdown.png"..efectcolor, --cima
		"tex_mailbox_topdown.png"..efectcolor, --baixo
		"tex_mailbox_sides.png"..efectcolor, --direita
		"tex_mailbox_sides.png^[transformFX"..efectcolor, --esquerda
		"tex_mailbox_back.png"..efectcolor, --traz
		"tex_mailbox_from.png^tex_mailbox_from_en.png"..efectcolor, --frente
		--
	},
}

--###############################################################################################################

minetest.register_node("correio:mailbox", {
	description = minetest.colorize("#FFFF00", modCorreio.translate("Private Mailbox")).."\n"
		.."   * "..modCorreio.translate("Displays received letters (accessible by owner only)"),
	inventory_image = "obj_mailbox.png",
	--inventory_image = minetest.inventorycube("tex_light.png"),
	drawtype = "nodebox",
	paramtype = "light",
	--paramtype2 = "fixed",
	paramtype2 = "facedir",
	walkable = false,
	node_box =mailbox_format,
	selection_box = mailbox_format,
	tiles = modCorreio.tiles.mailbox_private,
	use_texture_alpha = "clip",
	--sunlight_propagates = true,
	--light_source = LIGHT_MAX,
	is_ground_content = true, --Nao tenho certeza: Se prega no chao?
	groups = {snappy=2,choppy=2,oddly_breakable_by_hand=2,mailbox=1}, --{cracky=3,oddly_breakable_by_hand=3},
	sounds = default.node_sound_wood_defaults(), --default.node_sound_glass_defaults(),
	after_place_node = function(pos, placer, itemstack)
		if minetest.is_player(placer) then
   		local meta = minetest.get_meta(pos)
   		local owner = placer:get_player_name()
   		meta:set_string("infotext", modCorreio.translate("'%s' Mailbox"):format(owner))
   		meta:set_string("owner",owner)
   	end
	end,
	can_dig = function(pos, player)
	   if minetest.is_player(player) then
   		local meta = minetest.get_meta(pos)
   		if meta~=nil then
   			local ownername = meta:get_string("owner")
   			local playername = player:get_player_name()
				local playerpos = player:get_pos()
   			if (
						ownername~=nil and ownername~="" and ownername==playername
					)
					or (
						minetest.get_modpath("tradelands") 
						and minetest.global_exists("modTradeLands")
						and modTradeLands.getOwnerName(playerpos)~="" 
						and modTradeLands.canInteract(playerpos, playername)
					)
				then
   				return true
   			else
   				minetest.chat_send_player(playername, modCorreio.translate("You can not destroy the '%s' mailbox!"):format(ownername))
   				return false
   			end
   		else
   			return true
   		end
		else
			return false
		end
	end,
	on_rightclick = function(pos, node, clicker, itemstack)
		if minetest.is_player(clicker) then
   		local meta = minetest.get_meta(pos)
   		if meta~=nil then
   			local ownername = meta:get_string("owner")
   			if ownername~=nil and ownername~="" then
   				local clickername = clicker:get_player_name()
   				if ownername==clickername then
   					modCorreio.openinbox(ownername)
   				else
   					minetest.chat_send_player(clickername, modCorreio.translate("This mailbox belongs to '%s'!"):format(ownername))
   				end
   			end
   		end
   	end
	end,
})

minetest.register_craft({
	output = 'correio:mailbox',
	recipe = {
		{"group:wood"	,"group:wood"			,"group:wood"},
		{"group:wood"	,"correio:papermail"	,"group:wood"},
		{"group:wood"	,"group:wood"			,"group:wood"},
	}
})

minetest.register_alias(modCorreio.translate("mailbox")		,"correio:mailbox")

--###############################################################################################################

minetest.register_node("correio:mailbox_public", {
	description = minetest.colorize("#FFFF00", modCorreio.translate("Public Mailbox")).."\n"
		.."   * "..modCorreio.translate("Displays received letters (accessible to anyone)"),
	inventory_image = "obj_mailbox.png"..efectcolor,
	--inventory_image = minetest.inventorycube("tex_light.png"),
	drawtype = "nodebox",
	paramtype = "light",
	--paramtype2 = "fixed",
	paramtype2 = "facedir",
	walkable = false,
	node_box =mailbox_format,
	selection_box = mailbox_format,
	tiles = modCorreio.tiles.mailbox_public,
	use_texture_alpha = "clip",
	sunlight_propagates = true,
	light_source = LIGHT_MAX,
	is_ground_content = true, --Nao tenho certeza: Se prega no chao?
	groups = {snappy=2,choppy=2,oddly_breakable_by_hand=2,mailbox=1}, --{cracky=3,oddly_breakable_by_hand=3},
	sounds = default.node_sound_wood_defaults(), --default.node_sound_glass_defaults(),
	on_rightclick = function(pos, node, clicker, itemstack)
		if minetest.is_player(clicker) then
		   modCorreio.openinbox(clicker:get_player_name())
		end
	end,
})

minetest.register_alias(modCorreio.translate("mailboxpublic")		,"correio:mailbox_public")
