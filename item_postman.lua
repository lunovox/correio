if minetest.get_modpath("mobs") 
	and minetest.get_modpath("mobs_npc") 
	--and minetest.global_exists("mobs_npc")
then
	modCorreio.postman = {
	   trade_drops = { --Drop when pay.
			{name = "correio:papermail", min = 1, max = 2},			
		},
	}

	mobs:register_mob("correio:postman", {
		type = "npc",
		--nametag = modCorreio.translate("Postman"),
		passive = false,
		reach = 2,
		damage = 5,
		attack_type = "dogfight",
		attack_monsters = true,
		attack_npcs = false,
		owner_loyal = true,
		pathfinding = true,
		hp_min = 30,
		hp_max = 30,
		armor = 100,
		collisionbox = {-0.35,-1.0,-0.35, 0.35,0.8,0.35},
		visual = "mesh",
		mesh = "mobs_character.b3d",
		drawtype = "front",
		textures = {
			{"tex_postman.png"},
		},
		--[[
		child_texture = {
			{"tex_postman.png"} -- derpy baby by AmirDerAssassine
		},
		--]]
		makes_footstep_sound = true,
		sounds = {
			distance = "15",			-- Sons de distância máxima podem ser ouvidos, o padrão é 10.
		   --random = "sfx_postman_whistle_q3_112kbps",			-- Som aleatório que toca durante o jogo.
		   random = "sfx_talker_man",			-- Som aleatório que toca durante o jogo.
		   --war_cry = "",			-- o que você ouve quando o mob começa a atacar o jogador.
		   --attack = "",			-- O que você ouve quando é atacado.
		   --shoot_attack = "",	-- Som tocado quando o mob atira.
		   --damage = "",			-- Som ouvido quando o mob está ferido.
		   death = "sfx_postman_death_q3_112kbps",				-- Played when mob is killed.
		   jump = "sfx_jump_sonic",				-- Played when mob jumps.
		   --fuse = "",				-- Sound played when mob explode timer starts.
		   --explode = "",			-- Sound played when mob explodes.
		},
		walk_velocity = 2,
		run_velocity = 3,
		jump = true,
		--jump_height = 2,
		drops = { --Drop when death.
			{name = "correio:papermail", chance = 20, min = 1, max = 3},
		},
		water_damage = 1,
		lava_damage = 2,
		light_damage = 0,
		follow = {
			"correio:papermail",
		},
      stay_near = { --when set allows mobs the chance to stay around certain nodes.
         { --'node' : string or table of nodes to stay nearby e.g. "farming:straw"
            "group:mailbox",
         }, 
         5 , --'chance': chance of searching for above node(s), default is 10.
      },
      runaway_from = {
         "dog:dog",
         "mobs_animal:chicken",
      },
		
		view_range = 20,
		owner = "",
		order = "wander",
		fear_height = 3,
		animation = {
			speed_normal = 30,
			speed_run = 30,
			stand_start = 0,
			stand_end = 79,
			walk_start = 168,
			walk_end = 187,
			run_start = 168,
			run_end = 187,
			punch_start = 189, --200
			punch_end = 198 --219
		},


		
		
		on_rightclick = function(self, clicker)

			-- feed to heal npc
			if mobs:feed_tame(self, clicker, 8, true, true) then return end

			-- capture npc with net or lasso
			if mobs:capture_mob(self, clicker, nil, 5, 80, false, nil) then return end

			-- protect npc with mobs:protector
			if mobs:protect(self, clicker) then return end

			local itemWielded = clicker:get_wielded_item()
			local playername = clicker:get_player_name()
			local who = minetest.colorize("#00FF00", modCorreio.translate("Postman"))..": "

			--[[ --]]
			-- right clicking with gold lump drops random item from mobs.npc_drops
			if 
				itemWielded:get_name() == "minertrade:minercoin"
				or itemWielded:get_name() == "mcl_raw_ores:raw_gold"
				or itemWielded:get_name() == "default:gold_lump"
			then
				if not mobs.is_creative(playername) then
					itemWielded:take_item()
					clicker:set_wielded_item(itemWielded)
				end
				local pos = self.object:get_pos()
				
				--minetest.chat_send_all(dump(self.trade_drops))
				--return nil
				
				local dropSelected = self.drops[math.random(1, #self.drops)]
				local dropName = "default:paper"
				local dropCount = 1
				if type(dropSelected) == "table" 
				   and type(dropSelected.name)=="string" and dropSelected.name ~= ""
					and minetest.registered_items[dropSelected.name]
					and type(dropSelected.min)=="number" and dropSelected.min >=1
					and type(dropSelected.max)=="number" and dropSelected.max >=1
					and dropSelected.min <= dropSelected.max
				then
					dropName = dropSelected.name
					dropCount = math.random(dropSelected.min, dropSelected.max)
				end
				
				local obj = minetest.add_item(pos, {name = dropName, count = dropCount})
				local dir = clicker:get_look_dir()
				obj:set_velocity({x = -dir.x * 1.5, y = 1.5, z = -dir.z * 1.5})
				--minetest.chat_send_player(playername, S("NPC dropped you an item for gold!"))
				
				local talkSelected = 0
				local talkColections
				talkSelected = math.random(1, 2)
				talkColections = {
					who..modCorreio.translate("Thank you for purchasing my letter packets."),
					who..modCorreio.translate("This little money makes my workday a lot easier."),
				}
				self.state = "stand"
				self:set_animation("stand")
				self.object:set_yaw(clicker:get_look_horizontal() + math.pi)
				minetest.chat_send_player(playername, talkColections[talkSelected])
				minetest.sound_play("sfx_jump_sonic", {
					to_player = playername, 
					gain = 1.0,
					max_hear_distance=5.0,
				})
				return
			end
			--]]

			--[[ --]]
			-- by right-clicking owner can switch npc between follow, wander and stand
			if 
				itemWielded:get_name() == "default:paper"
				and (
					self.owner == playername 
					or minetest.check_player_privs(clicker, {protection_bypass = true}) 
				)
			then
				if self.order == "follow" then
					self.order = "wander"
					minetest.chat_send_player(playername, who..modCorreio.translate("I will wander."))
				elseif self.order == "wander" then
				   self.object:set_yaw(clicker:get_look_horizontal() + math.pi)
					self.order = "stand"
					self.state = "stand"
					self.attack = nil
					self:set_animation("stand")
					self:set_velocity(0)
					minetest.chat_send_player(playername, who..modCorreio.translate("I stands still."))
				elseif self.order == "stand" then
					self.object:set_yaw(clicker:get_look_horizontal() + math.pi)
					self.order = "follow"
					minetest.chat_send_player(playername, who..modCorreio.translate("I will follow you."))
				end
				minetest.sound_play("sfx_talker_man", {
					to_player = playername, 
					gain = 1.0,
					max_hear_distance=5.0,
				})
				return
			end
			--]]
			
			
			local unreadeds = modCorreio.get_countunreaded(playername)
			if unreadeds >= 1 then
			   self.object:set_yaw(clicker:get_look_horizontal() + math.pi)
			   minetest.chat_send_player(playername, 
			      who..modCorreio.translate("Correio! Here are your unread letters!")
			   )
			   minetest.sound_play("sfx_postman_correio_q3_112kbps", {
					to_player = playername, 
					max_hear_distance=5.0,
				})
				modCorreio.openinbox(playername)
			else
   			local talkSelected = 0
   			local talkColections
   			if
   				self.owner == playername 
   				or minetest.check_player_privs(clicker, {protection_bypass = true}) 
   			then
   				talkSelected = math.random(1, 10)
   				talkColections = {
   					who..modCorreio.translate("My dream is to avoid fatigue by gaining a level 2 rune from @1.", minetest.colorize("#00FF00", playername)),
   					who..modCorreio.translate("I need to complete the deliveries, or I won't get paid this month."), 
   					who..modCorreio.translate("Since we are friends, use a blank paper to give me instructions."),
   					who..modCorreio.translate("I'm afraid of scary animals walking around the letter boxes."),
   					who..modCorreio.translate("The salary I receive is not enough for all this fatigue."), 
   					who..modCorreio.translate("You can give me a nickname if you give me a nametag."),
   					who..modCorreio.translate("If you pay me, I will give you some paper letters!"),
   					who..modCorreio.translate("I'm resting because I want to avoid fatigue!"), 
   					who..modCorreio.translate("When you receive a letter, come talk to me!"),
   					who..modCorreio.translate("Don't forget to cage your scary animals."),
   				}
   			else
   				talkSelected = math.random(1, 3)
   				talkColections = {
   					who..modCorreio.translate("I'm kind of busy avoiding scary animals."), 
   					who..modCorreio.translate("Keep out, boring!! We only can be friends if you give me some letter envelopes."),
   					who..modCorreio.translate("I earn too little to pay attention to you, a slutty people."),  
   				}
      		end
				self.state = "stand"
				self:set_animation("stand")
				self.object:set_yaw(clicker:get_look_horizontal() + math.pi)
				minetest.chat_send_player(playername, talkColections[talkSelected])
   			minetest.sound_play("sfx_talker_man", {
   				to_player = playername, 
   				gain = 1.0,
   				max_hear_distance=5.0,
   			})
			end
			
		end
		
		




		--[[
		on_rightclick = function(self, clicker)
			-- feed to heal npc
			if mobs:feed_tame(self, clicker, 8, true, true) then return end

			-- capture npc with net or lasso
			if mobs:capture_mob(self, clicker, nil, 5, 80, false, nil) then return end

			-- protect npc with mobs:protector
			if mobs:protect(self, clicker) then return end

			local itemwield = clicker:get_wielded_item()
			local playername = clicker:get_player_name()

			

			-- owner can right-click with stick to show control formspec
			if itemwield:get_name() == "" then
				-- show simple dialog if enabled or idle chatter
				if mobs_npc.useDialogs == "Y" then
					simple_dialogs.show_dialog_formspec(playername, self)
				else
					local who = minetest.colorize("#00FF00", modCorreio.translate("Postman"))..": "
					if self.state == "attack" then
						mobs_npc.npc_talk(self, clicker, {
							who..modCorreio.translate("I need to make deliveries."), 
							who..modCorreio.translate("Keep out, boring!!"), 
							who..modCorreio.translate("I'm kinda busy."), 
							who..modCorreio.translate("Keep out, boring!!"),
							who..modCorreio.translate("I earn too little to pay attention to ugly people."),  
						})
						minetest.sound_play("sfx_talker_man", {
							to_player = playername, 
							max_hear_distance=5.0,
						})
					else
						local myAction = math.random(1, 15)
						
						if myAction == 1 then
							if minetest.is_player(clicker) then
								minetest.sound_play("sfx_postman_correio_q3_112kbps", {
									to_player = playername, 
									max_hear_distance=5.0,
								})
								modCorreio.openinbox(playername)
							end
						else
							mobs_npc.npc_talk(self, clicker, {
								who..modCorreio.translate("I need to make deliveries."), 
								who..modCorreio.translate("The salary I receive is not enough."), 
								who..modCorreio.translate("I'm afraid of dogs."),
								who..modCorreio.translate("Don't forget to put a collar on your dog."),
								who..modCorreio.translate("When you receive a mailing notice, come talk to me!"),
								who..modCorreio.translate("When you receive a mailing notice, come talk to me!"),
								who..modCorreio.translate("If you pay me, I will give you some paper letters!"),
							})
							minetest.sound_play("sfx_talker_man", {
								to_player = playername, 
								max_hear_distance=5.0,
							})
						end
					end
					
				end
			elseif itemwield:get_name() == (mcl and "mcl_core:stick" or "default:stick")
				and (
					self.owner == playername 
					or minetest.check_player_privs(clicker, {protection_bypass = true}) 
				)
			then
				minetest.show_formspec(
					playername, "mobs_npc:controls",
					mobs_npc.get_controls_formspec(playername, self)
				)
				return
			elseif itemwield:get_name() == "minertrade:minercoin"
					or itemwield:get_name() == "mcl_raw_ores:raw_gold"
					or itemwield:get_name() == "default:gold_lump"
			then
				-- right clicking with gold lump drops random item from list
				if mobs_npc.drop_trade(
					self, 
					clicker, 
						(
							minetest.get_modpath("minertrade") and "minertrade:minercoin"
						) 
						or (
							mcl and "mcl_raw_ores:raw_gold"
						) 
						or "default:gold_lump", 
					--self.npc_drops or mobs_npc.npc_drops
					{
						{"correio:papermail"},
					}
				) then
					mobs_npc.npc_talk(self, clicker, {
						modCorreio.translate("Thank you for purchasing my letter packets."),
						modCorreio.translate("This little money makes my workday a lot easier."),
					})
					return
				end
			end
		end
		--]]
		
		
		
		
		
	})
	
	-- register spawn egg
	mobs:register_egg(
		"correio:postman", 
		modCorreio.translate("Postman"),
		"obj_mail.png^[resize:16x16"
		, 1 --'addegg' = would you like an egg image in front of your texture (1 = yes, 0 = no)
	)


	-- this is only needed for servers that used the old mobs mod
	mobs:alias_mob("mobs:postman", "correio:postman")


   --if not mobs.custom_spawn_npc then
      mobs:spawn({
      	name = "correio:postman",
      	nodes = {
            "group:mailbox", 
      	},
      	neighbors = {"air"},
      	min_light = 10,
      	min_height = 0,
      	max_height = 100,
      	chance = 100, --Defaul 10000
      	active_object_count = 1,
      	interval = 30,
      	day_toggle = true
      })
		
		--[[ --EXEMPLO DE MOB SPAWN:
		mobs:spawn({
			name = "mobs_npc:npc",
			nodes = {"default:brick"},
			neighbors = {"default:grass_3"},
			min_light = 10,
			chance = 10000,
			active_object_count = 1,
			min_height = 0,
			day_toggle = true
		})
		--]]
   --end

end
